# CSS_Sprite_Generator

## Introduction

> A tool written in PHP that allows you to generate sprite images along with their css.

## Code Samples

```php 
function glob_recursive($base, $pattern, $flags = 0)
{
    if (substr($base, -1) !== DIRECTORY_SEPARATOR) {
        $base .= DIRECTORY_SEPARATOR;
    }

    $files = glob($base . $pattern, $flags);

    foreach (glob($base . '*', GLOB_ONLYDIR | GLOB_NOSORT | GLOB_MARK) as $dir) {
        $dirFiles = glob_recursive($dir, $pattern, $flags);
        if ($dirFiles !== false) {  
            $files = array_merge($files, $dirFiles);
        }
    }

    return $files;
```

## Installation

>PHP 7 and GD are required.